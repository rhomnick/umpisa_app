
# Umpisa App

## A Webpage Implementation for Umpisa Inc.'s ReactJS & GraphQL Test / Study

  
  
  

## Install

  
```
$ git clone git clone https://bitbucket.org/rhomnick/umpisa_app.git
```
```
$ cd umpisa_app
```
```
$ npm install
```

  

### Starting the app
run
```
$ npm start
```
Visit [http://localhost:9001/posts](http://localhost:9001/posts) on your local machine
**Take note that the web app needs an API to get its content from**

## Login Credentials
To login to the web app please use the following 
```
email: nick@email.com
password: password
```
## Unit Testing
Check weather all compnents work as expected
```
$ npm run test
```
## The API (run this one first)

```
$ git clone git clone https://bitbucket.org/rhomnick/umpisa_app_api.git
```
```
$ cd umpisa_app_api
```
```
$ npm install
```

**Before starting the api server, let's run a mongodb in you local Docker**
```
$ cd mongo/
```
```
$ docker-compose up -d
```
**Seed database**
This is needed to populate inital datas to the database

```
$ npm run seed
```
**now we can start our api server**
```
$ npm start
```

**Dockerizing the app**
Should you need to dockerize the app, a Dockerfile is included in the root folder

### Enjoy!

  

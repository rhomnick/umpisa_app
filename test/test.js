
import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'
Enzyme.configure({ adapter: new Adapter() });


import PostCard from '../src/components/App/PostCard';
import Todo from '../src/components/App/Todos/Todo';
import {
  Card
} from 'react-bootstrap'

describe('<PostCard/> Without Image', () => {

  const post = shallow(
    <PostCard
      post={{
        primaryImage: '/ss',
        title: 'Title ',
        content: '_content',
        author: { firstName: 'Rhomnick', lastName: '' }
      }}
    />
  );
  it('renders successfully', () => {
    expect(post.text()).toEqual('Title Posted by: Rhomnick _content')
  });
});

describe('<PostCard/>', () => {
  const primaryImage = 'https://mumbrella.com.au/wp-content/uploads/2018/07/Australia-Post-red-post-box.jpeg';
  const wrapper = shallow(
    <PostCard
      post={{
        primaryImage,
        title: 'WithImage Title ',
        content: 'WithImageContent',
        author: { firstName: 'Rhomnick', lastName: '' }
      }}
      withImage
    />
  );
  it('renders successfully', () => {
    expect(wrapper.find(Card.Img)).toHaveLength(1)
    expect(wrapper.text()).toEqual('WithImage Title Posted by: Rhomnick WithImageContent')
  });
});

describe('<Todo/>', () => {
  const wrapper = shallow(
    <Todo
      todo={{
        title: 'Play Drums Tonight'
      }}
    />
  );
  it('renders successfully', () => {
    expect(wrapper.text()).toEqual('Play Drums Tonight')
  });
});
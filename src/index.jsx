// Core
import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
// import { ApolloProvider } from 'react-apollo'
import { Provider } from 'react-redux'

// Apollo client
// import client from 'appModules/app/apollo'

// Store
import store from 'appModules/app/store'

// Components
import App from 'appComponents/App'


const renderComponent = (Component) => {
  ReactDOM.render(
    // <ApolloProvider client={client}>
      <Provider store={store}>
        <BrowserRouter
          basename=""
        >
          <Component />
        </BrowserRouter>
      </Provider>,
    // </ApolloProvider>,
    document.getElementById('root'),
  )
}

renderComponent(App)

// Core
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import {
  Row, Col
} from 'react-bootstrap'

// Operations
import { readProductsOperations } from 'appModules/products/read_products'

// Components
import PostCard from './PostCard'

@connect(state => ({
  products: state.products,
}))
class Products extends React.Component {
  static propTypes = {
    products: PropTypes.object,
    dispatch: PropTypes.func,
  }

  static defaultProps = {
    products: {},
    dispatch: f => f,
  }

  componentDidMount() {
    const { dispatch } = this.props
    dispatch(readProductsOperations.readProducts(
      {
        "where": {
          "typeLiteral": "products"
        },
        "offset": 0,
        "limit": 1000
      }
    ))
  }

  render() {
    const { products } = this.props

    if (products.result) {
      let list = products.result.map(postId => (
        <PostCard bgColor="secondary" txtColor="white" withImage key={postId} post={products.data[postId]} style={{ textAlign: 'center' }} />
      ))
      return (
        <Row>
          <Col xs={12} md={12}>
            <br />
            <h1>Products</h1>

          </Col>
          <Col xs={12} md={12}>
            {list}
          </Col>
        </Row>
      )
    }

    return 'no products'
  }
}


export default Products

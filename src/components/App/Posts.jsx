// Core
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import {
  Row, Col
} from 'react-bootstrap'

// Operations
import { readPostsOperations } from 'appModules/posts/read_posts'

// Components
import PostCard from './PostCard'

@connect(state => ({
  posts: state.posts,
}))
class Posts extends React.Component {
  static propTypes = {
    posts: PropTypes.object,
    dispatch: PropTypes.func,
  }

  static defaultProps = {
    posts: {},
    dispatch: f => f,
  }

  componentDidMount() {
    const { dispatch } = this.props
    dispatch(readPostsOperations.readPosts(
      {
        "where": {
          "typeLiteral": "posts"
        },
        "offset": 0,
        "limit": 10
      }
    ))
  }

  render() {
    const { posts } = this.props

    if (posts.result) {
      let list = posts.result.map(postId => (
        <PostCard bgColor="white" txtColor="black" key={postId} post={posts.data[postId]} style={{ textAlign: 'center' }} />
      ))
      return (
        <Row>
          <Col xs={12} md={12}>
            <br />
            <h1>Posts</h1>

          </Col>
          <Col xs={12} md={12}>
            {list}
          </Col>
        </Row>
      )
    }

    return 'no posts'
  }
}


export default Posts

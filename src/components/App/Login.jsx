// Core
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {
  Row, Col, Card, Form, Button, Div
} from 'react-bootstrap'
import { Login as LoginIcon, UserAdd } from 'grommet-icons'

// Operations
import { createUserOperations } from 'appModules/users/create_user'
import { readUserOperations } from 'appModules/users/read_user'

@withRouter
@connect(state => ({
  createUserForm: state.createUserForm,
  readUserForm: state.readUserForm,
}))
class Login extends React.Component {
  static propTypes = {
    createUserForm: PropTypes.object,
    readUserForm: PropTypes.object,
    history: PropTypes.object,
  }

  static defaultProps = {
    createUserForm: {},
    readUserForm: {},
  }

  state = {
    email: '',
    password: '',
    confirmPassword: '',
  }

  componentDidUpdate(prevProps) {
    const {
      createUserForm: { createUserSucceeded },
      readUserForm: { readUserSucceeded },
      history,
    } = this.props

    if (prevProps.createUserForm.createUserSucceeded !== createUserSucceeded && createUserSucceeded
      || prevProps.readUserForm.readUserSucceeded !== readUserSucceeded && readUserSucceeded) {
      history.push('/posts')
    }
  }

  onLoginButtonClick = (e) => {
    const email = e.target[0].value
    const password = e.target[1].value
    const { dispatch } = this.props
    dispatch(readUserOperations.readUser(
      {
        email,
        password
      }
    ))
    e.preventDefault();
  }

  onRegisterButtonClick = (e) => {
    const { value } = e
    return e.preventDefault();
    const { dispatch } = this.props
    const {
      email,
      password,
      confirmPassword,
    } = value

    if (password === confirmPassword) {
      dispatch(createUserOperations.createUser(
        email,
        password,
      ))
    }
  }

  render() {
    return (
      <Row>
        <Col xs={4} md={4}>&nbsp;</Col>
        <Col xs={4} md={4}>
          <Col style={{ margin: "50px 0" }}>
            <Form onSubmit={this.onLoginButtonClick}>
              <h2> Login </h2>
              <br/>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control name="email" type="email" placeholder="Enter email" />
                <Form.Text className="text-muted">
                  You email is safe with us.
          </Form.Text>
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control name="password" type="password" placeholder="Password" />
              </Form.Group>
              <Button variant="primary" type="submit">
                Submit
        </Button>
            </Form>
          </Col>
        </Col>
        <Col xs={4} md={4}>&nbsp;</Col>
      </Row>
      // <Box
      //   align="center"
      //   justify="center"
      //   fill
      //   direction="column"
      // >
      //   <Tabs>
      //     <Tab title="LOGIN">
      //       <Box 
      //         direction="row"
      //         justify="center"
      //         align="center"
      //         gap="xsmall"
      //       >
      //         <LoginIcon size="medium" />
      //         <Heading level={2}>
      //           LOGIN
      //         </Heading>  
      //       </Box>
      //       <Form onSubmit={this.onLoginButtonClick}>
      //         <FormField name="email" label="Email" required />
      //         <FormField type="password" name="password" label="Password" required />
      //         <Button 
      //           primary 
      //           type="submit" 
      //           label="Login"
      //         />
      //       </Form>
      //     </Tab>
      //     <Tab title="REGISTER">
      //       <Box 
      //         direction="row"
      //         justify="center"
      //         align="center"
      //         gap="xsmall"
      //       >
      //         <UserAdd size="medium" />
      //         <Heading level={2}>
      //           REGISTER
      //         </Heading>  
      //       </Box>
      //       <Form onSubmit={this.onRegisterButtonClick}>
      //         <FormField name="email" label="Email" required />
      //         <FormField type="password" name="password" label="Password" required />
      //         <FormField type="password" name="confirmPassword" label="Confirm Password" required />
      //         <Button 
      //           primary 
      //           type="submit" 
      //           label="Register"
      //         />
      //       </Form>
      //     </Tab>
      //   </Tabs>
      // </Box>
    )
  }
}


export default Login

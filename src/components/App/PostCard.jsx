// Core
import React from 'react'
import PropTypes from 'prop-types'
import {
  Card
} from 'react-bootstrap'


const PostCard = ({ post, withImage, bgColor, txtColor }) => {

  return (
    <Card style={{ width: '30%', display: 'inline-block', verticalAlign: 'top', margin: '15px' }} bg={bgColor} text={txtColor}>
      {withImage ? <Card.Img className="post-img" variant="top" src={post.primaryImage} /> : null}
      <Card.Body>
        <Card.Title as="h2">{post.title}</Card.Title>
        <hr />
        <Card.Text as="small">Posted by: {post.author.firstName} {post.author.lastName}</Card.Text>
        <hr />
        <Card.Text as="p">{post.content}</Card.Text>
      </Card.Body>
    </Card>

  )
}

PostCard.propTypes = {
  post: PropTypes.object.isRequired,
}


export default PostCard

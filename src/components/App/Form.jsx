import React from 'react';
import { Formik, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup'
import {
  Row, Col, Button, Form, Card
} from 'react-bootstrap'

const CreateForm = ({ handleCreate }) => (
  <div>
    <Formik
      initialValues={{ title: '', content: 'todo' }}
      validationSchema={Yup.object().shape({
        title: Yup.string()
          .required('This field is required!'),
      })}
      onSubmit={(values, { setSubmitting, resetForm }) => {
        handleCreate(values)
        setTimeout(() => {
          setSubmitting(false);
          resetForm()
        }, 400);
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        /* and other goodies */
      }) => (
          <Card bg="white" text="blalck">
            <Card.Body>
              <Form onSubmit={handleSubmit}>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Email address</Form.Label>
                  <Field
                    type="text"
                    name="title"
                    placeholder="Enter title"
                    className={`form-control ${touched.title && errors.title ? "is-invalid" : ""}`}
                  />
                  <ErrorMessage
                    component="div"
                    name="title"
                    className="invalid-feedback"
                  />
                </Form.Group>
                <Button
                  variant="primary"
                  type="submit"
                  disabled={isSubmitting}
                >Submit</Button>
              </Form>
            </Card.Body>
          </Card>
          // <form onSubmit={handleSubmit}>
          //   <input
          //     type="email"
          //     name="email"
          //     onChange={handleChange}
          //     onBlur={handleBlur}
          //     value={values.email}
          //   />
          //   {errors.email && touched.email && errors.email}
          //   <input
          //     type="password"
          //     name="password"
          //     onChange={handleChange}
          //     onBlur={handleBlur}
          //     value={values.password}
          //   />
          //   {errors.password && touched.password && errors.password}
          //   <button type="submit" disabled={isSubmitting}>
          //     Submit
          // </button>
          // </form>
        )}
    </Formik>
  </div>
);

export default CreateForm;
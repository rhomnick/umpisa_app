// Core
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import {
  Row, Col, ListGroup
} from 'react-bootstrap'

// Operations
import { readTodosOperations } from 'appModules/todos/read_todos'
import { createTodosOperations } from 'appModules/todos/create_todos'

// Components
import Todo from './Todos/Todo'
import CreateForm from './Form'

@connect(state => ({
  todos: state.todos,
  createTodosForm: state.createTodosForm,
}))
class Todos extends React.Component {
  static propTypes = {
    todos: PropTypes.object,
    dispatch: PropTypes.func,
  }

  static defaultProps = {
    todos: {},
    dispatch: f => f,
    createTodosForm: {},
  }

  componentDidMount() {
    const { dispatch } = this.props
    dispatch(readTodosOperations.readTodos(
      {
        "where": {
          "typeLiteral": "todos"
        },
        "offset": 0,
        "limit": 1000
      }
    ))
  }

  handleCreate = (data) => {
    let newData = {
      ...data,
      typeLiteral: "todos"
    }
    const { dispatch } = this.props
    dispatch(createTodosOperations.createTodos(
      { data: newData }
    ))
  }

  render() {
    const { todos } = this.props
    let list = <div>
      <h4 className="">You have no todos at the moment</h4>
      <h6 className="">Create by filling up the form at the right side.</h6>
    </div>
    if (todos.result && todos.result.length > 0)
      list = todos.result.map(todoId => (
        <Todo bgColor="primary" key={todoId} todo={todos.data[todoId]} style={{ textAlign: 'center' }} />
      ))
    return (
      <Row>
        <Col xs={12} md={12}>
          <br />
          <h1>Todos</h1>
          <hr />
        </Col>
        <Col xs={12} md={8}>
          <ListGroup >
            {list}
          </ListGroup>
        </Col>
        <Col xs={12} md={4}>
          <h4>Create New</h4>
          <CreateForm handleCreate={this.handleCreate} />
        </Col>
      </Row>
    )

  }
}


export default Todos

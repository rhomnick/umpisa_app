// Core
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import {
  Row, Col, Card, Jumbotron, Container, Button
} from 'react-bootstrap'

// Operations
import { logoutOperations } from 'appModules/auth/logout'

// Components
@withRouter
@connect(state => ({
  user: state.user,
}))
class Profile extends React.Component {
  static propTypes = {
    user: PropTypes.object,
    dispatch: PropTypes.func,
    history: PropTypes.object,
  }

  static defaultProps = {
    user: {},
    history: {},
    dispatch: f => f,
  }

  onLogoutBtnClick = () => {
    const { dispatch, history } = this.props
    dispatch(logoutOperations.logout())
    history.push(`/login`)
  }

  render() {
    const { user } = this.props
    if (user) {
      return (
        <div>

          <Jumbotron>
            <Container>
              <Row>
                <Col xs={12} md={2}>
                  <Card.Img
                    variant="top" src="https://www.belightsoft.com/products/imagetricks/img/intro-video-poster@2x.jpg"
                    style={{
                      width: "150px",
                      height: "150px",
                      borderRadius: 100
                    }}
                  />
                </Col>
                <Col xs={12} md={10}>
                  <div className="d-inline">
                    <h1 className="display-3 ">{user.firstName} {user.lastName}</h1>
                    <Button onClick={this.onLogoutBtnClick} variant="danger">Logout</Button>
                  </div>
                </Col>
              </Row>
            </Container>
          </Jumbotron>
          <h2>Analytics</h2>
          Total Posts counts
          <hr />
          {user.analytics &&
            <Container>
              <Card bg="info" text="white" style={{ width: '30%', display: 'inline-block', verticalAlign: 'top', margin: '15px' }}>
                <Card.Header>Posts</Card.Header>
                <Card.Body>
                  <Card.Title>{user.analytics.posts}</Card.Title>
                </Card.Body>
              </Card>
              <Card bg="warning" text="white" style={{ width: '30%', display: 'inline-block', verticalAlign: 'top', margin: '15px' }}>
                <Card.Header>Products</Card.Header>
                <Card.Body>
                  <Card.Title>{user.analytics.products}</Card.Title>
                </Card.Body>
              </Card>
              <Card bg="danger" text="white" style={{ width: '30%', display: 'inline-block', verticalAlign: 'top', margin: '15px' }}>
                <Card.Header>Todos</Card.Header>
                <Card.Body>
                  <Card.Title>{user.analytics.todos ? user.analytics.todos : 0}</Card.Title>
                </Card.Body>
              </Card>
            </Container>
          }
        </div >
      )
    }

    return ''
  }
}


export default Profile

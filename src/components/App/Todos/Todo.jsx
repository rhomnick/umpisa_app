// Core
import React from 'react'
import PropTypes from 'prop-types'
import {
  ListGroup
} from 'react-bootstrap'


const Todo = ({ todo }) => {
  return (
    <ListGroup.Item>
      <h6 level={3}>{todo.title}</h6>
    </ListGroup.Item>
  )
}

Todo.propTypes = {
  todo: PropTypes.object.isRequired,
}


export default Todo

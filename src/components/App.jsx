// Core
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Redirect, Route, Switch } from 'react-router-dom'
import {
  Container, Row, Col, Navbar, Nav, Form, FormControl, Button
} from 'react-bootstrap'

// Components
import Posts from './App/Posts'
import Products from './App/Products'
import Todos from './App/Todos'
import Profile from './App/Profile.jsx'
import Login from './App/Login.jsx'

// Operations
import { logoutOperations } from 'appModules/auth/logout'
import { readUserOperations } from 'appModules/users/read_user'

// Styles
import './App.scss'
import 'bootstrap/dist/css/bootstrap.min.css';

@withRouter
@connect(state => ({
  user: state.user,
}))
class App extends React.Component {
  static propTypes = {
    user: PropTypes.object,

    history: PropTypes.object,

    dispatch: PropTypes.func.isRequired,
  }

  static defaultProps = {
    user: {},
    history: {},
    dispatch: f => f,
  }

  componentDidMount() {
    const { dispatch, user } = this.props
    this.onButtonClick('login')
    if (!user._id)
      dispatch(readUserOperations.readUserProfile({}))
  }

  componentDidUpdate() {
    const { dispatch, user, history: { location: { pathname } } } = this.props
    if (pathname === '/') {
      this.onButtonClick('posts')
    }
  }

  onButtonClick = (pathname) => {
    const { dispatch, history } = this.props
    history.push(`/${pathname}`)
  }

  render() {
    const {
      user,
      history: { location: { pathname } }
    } = this.props
    const jwtToken = localStorage.getItem('jwtToken')

    const isUserLoggedIn = !!jwtToken && user._id

    return (

      <Container fluid style={{ margin: 0, padding: 0, background: '#f9f9f9' }}>
        <Navbar bg="dark" variant="dark" sticky="top">
          <Navbar.Brand href="/">Umpisa App</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link className={pathname == '/posts' ? 'active' : ''} onClick={() => this.onButtonClick('posts')}>Posts</Nav.Link>
            <Nav.Link className={pathname == '/products' ? 'active' : ''} onClick={() => this.onButtonClick('products')}>Products</Nav.Link>
            <Nav.Link className={pathname == '/todos' ? 'active' : ''} onClick={() => this.onButtonClick('todos')}>Todos</Nav.Link>
          </Nav>
          <Form inline>
            {isUserLoggedIn
              ? <Nav.Link
                className={pathname == '/profile' ? 'active' : ''}
                style={{
                  background: '#505051',
                  color: '#fff',
                  borderRadius: '5px',
                  marginRight: '10px',
                  fontSize: 10
                }} onClick={() => this.onButtonClick('profile')}>View Profile</Nav.Link>
              : null
            }
            {isUserLoggedIn
              ? <div>
                <div
                  className="d-inline-block"
                  style={{
                    width: 40,
                    height: 40,
                    background: `url(${user.avatar}) 100%/cover`,
                    verticalAlign: 'middle',
                    borderRadius: 100,
                    marginRight: 5
                  }}
                ></div>
                <Navbar.Text display="inline-block">{user.firstName} {user.lastName}</Navbar.Text>
              </div>
              : <Nav.Link onClick={() => this.onButtonClick('login')}>Login</Nav.Link>
            }
          </Form>
        </Navbar>
        <Container style={{ scroll: 'auto' }}>
          <Row>
            <Col xs={12} md={12}>
              <Switch>
                <Route exact path="/posts">
                  <Posts />
                </Route>
                <Route exact path="/products">
                  <Products />
                </Route>
                <Route exact path="/todos">
                  <Todos />
                </Route>
                <Route exact path="/login">
                  <Login />
                </Route>
                <Route exact path="/profile">
                  <Profile />
                </Route>
                <Route exact path="/logout">
                  <Redirect to="/posts" />
                </Route>
              </Switch>
            </Col>
          </Row>
        </Container>

      </Container >
    )
  }
}


export default App

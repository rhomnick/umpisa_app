// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import readPostsReducer from './read_posts'


// State shape
const initialState = {
  posts: {},

  readPostsForm: {
    // Error
    errors: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingPosts: false,
    readPostsFailed: false,
    readPostsSucceeded: false,
  },
}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  readPostsReducer,
)

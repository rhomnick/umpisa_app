// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const readPostsReducer = (state, action) => {
  switch (action.type) {
    case types.READ_POSTS:
      return {
        ...state,

        readPostsForm: {
          ...state.readPostsForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingPosts: true,
          readPostsFailed: false,
          readPostsSucceeded: false,
        },
      }
    case types.READ_POSTS_FAIL:
      return {
        ...state,

        readPostsForm: {
          ...state.readPostsForm,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          readingPosts: false,
          readPostsFailed: true,
          readPostsSucceeded: false,
        },
      }
    case types.READ_POSTS_SUCCESS: {
      return {
        ...state,

        posts: action.posts,

        readPostsForm: {
          ...state.readPostsForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingPosts: false,
          readPostsFailed: false,
          readPostsSucceeded: true,
        },
      }
    }
    default:
      return state
  }
}


export default reduceReducers(readPostsReducer)

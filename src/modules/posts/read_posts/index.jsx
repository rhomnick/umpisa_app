import reducer from './reducers'

import * as readPostsOperations from './operations'

import readPostsEpic from './epic'


export {
  readPostsOperations,
  readPostsEpic,
}

export default reducer

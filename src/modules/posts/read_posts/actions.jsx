import * as types from './types'


export const readPosts = (data) => ({
  type: types.READ_POSTS,
  payload: data
})


export const readPostsFail = (data = {}, message = '', code = null) => ({
  type: types.READ_POSTS_FAIL,
  errors: {
    data,
    message,
    code,
  },
})


export const readPostsSuccess = posts => ({
  type: types.READ_POSTS_SUCCESS,
  posts,
})

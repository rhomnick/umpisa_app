// Core
import { combineEpics } from 'redux-observable'

// Auth
import { logoutEpic } from 'appModules/auth/logout'

// Users
import { createUserEpic } from 'appModules/users/create_user'
import { readUserEpic, readUserProfileEpic } from 'appModules/users/read_user'

// Posts
import { readPostsEpic } from 'appModules/posts/read_posts'

// Products
import { readProductsEpic } from 'appModules/products/read_products'

// Todos
import { readTodosEpic } from 'appModules/todos/read_todos'
import { createTodosEpic } from 'appModules/todos/create_todos'



export default combineEpics(
  logoutEpic,

  createUserEpic,
  readUserEpic,
  readUserProfileEpic,

  readPostsEpic,

  readProductsEpic,

  readTodosEpic,
  createTodosEpic,
)

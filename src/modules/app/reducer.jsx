// Core
import reduceReducers from 'reduce-reducers'

import { LOGOUT } from 'appModules/auth/logout/types'

// Auth
import authReducer from 'appModules/auth'

// Users
import usersReducer from 'appModules/users'

// Posts
import postsReducer from 'appModules/posts'

// Products
import productsReducer from 'appModules/products'

// Todos
import todosReducer from 'appModules/todos'

// State shape
const initialState = {}

// Initial reducer
const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGOUT:
      return {}
    default: 
      return state
  }
}

export default reduceReducers(
  appReducer,
  authReducer,
  usersReducer,
  postsReducer,
  productsReducer,
  todosReducer,
)

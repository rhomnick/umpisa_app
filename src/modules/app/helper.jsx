// eslint-disable-next-line import/prefer-default-export
export const normalize = (entities, identifier) => {
  const normalizedData = {}
  const normalizedResult = []

  entities.forEach((entity) => {
    normalizedData[entity[identifier]] = {
      ...entity,
    }
    normalizedResult.push(entity[identifier])
  })

  return {
    result: normalizedResult,
    data: normalizedData,
  }
}

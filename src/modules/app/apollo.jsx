import ApolloClient from 'apollo-boost'

const client = new ApolloClient({
  // LOCALHOST API
  uri: 'http://localhost:9000/api',
  
  // REMOTE API 
  // uri: 'http://3.1.25.175:9000/api',
  request: (operation) => {
    const token = localStorage.getItem('jwtToken');
    operation.setContext({
      headers: {
        slave: token,
        treat: navigator.userAgent,
        spoil: new Date().getTime()
      }
    });
  }
})

export default client

// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import logoutReducer from './logout'

// State shape
const initialState = {}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  logoutReducer,
)


import * as types from './types'

export const logout = () => ({
  type: types.LOGOUT,
})
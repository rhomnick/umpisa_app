// Core
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'

// Types
import { LOGOUT } from './types'


const logoutEpic = action$ => action$.pipe(
  ofType(LOGOUT),
  mergeMap(action => (
    Observable.create((observer) => {
      localStorage.removeItem('jwtToken')

      observer.complete()
    })
  )),
)


export default logoutEpic

import reducer from './reducers'

import * as logoutOperations from './operations'

import logoutEpic from './epic'


export {
  logoutOperations,
  logoutEpic,
}

export default reducer

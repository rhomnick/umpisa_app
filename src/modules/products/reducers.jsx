// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import readProductsReducer from './read_products'


// State shape
const initialState = {
  products: {},

  readProductsForm: {
    // Error
    errors: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingProducts: false,
    readProductsFailed: false,
    readProductsSucceeded: false,
  },
}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  readProductsReducer,
)

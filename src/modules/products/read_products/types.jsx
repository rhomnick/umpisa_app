export const READ_PRODUCTS = 'products/read_products/READ_PRODUCTS'
export const READ_PRODUCTS_FAIL = 'products/read_products/READ_PRODUCTS_FAIL'
export const READ_PRODUCTS_SUCCESS = 'products/read_products/READ_PRODUCTS_SUCCESS'

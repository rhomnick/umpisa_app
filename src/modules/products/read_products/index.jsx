import reducer from './reducers'

import * as readProductsOperations from './operations'

import readProductsEpic from './epic'


export {
  readProductsOperations,
  readProductsEpic,
}

export default reducer

// Core
// import api from 'appModules/app/axios'
// import camelcaseKeys from 'camelcase-keys'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import { normalize } from 'appModules/app/helper'
import moment from 'moment'

import { gql } from 'apollo-boost'
import client from 'appModules/app/apollo'

// Types
import { READ_PRODUCTS } from './types'

// Action
import { readProductsSuccess, readProductsFail } from './operations'


const readProductsEpic = action$ => action$.pipe(
  ofType(READ_PRODUCTS),
  mergeMap(action => (
    Observable.create((observer) => {
      client.query({
        query: gql`
              query (
                $where: RecordsInput
                $limit : Int
                $offset : Int
              ) {
                getRecords( 
                  where: $where,
                  limit : $limit,
                  offset : $offset
                ) {
                  count
                  offset
                  limit
                  data {
                    _id          
                    status
                    primaryImage
                    title
                    content
                    author{
                      _id
                      firstName
                      lastName
                    }

                  type{
                    _id
                    name
                    }
                  category{
                      _id
                      name
                    }
                  }
                }
              }          
        `,
        variables: action.payload,
        fetchPolicy: 'network-only'
      }).then((result) => {
        const productsData = result.data.getRecords
        const products = normalize(productsData.data.map(productData => ({
          ...productData,
          dateCreated: moment(+productData.createdAt).fromNow(),
        })), '_id')

        observer.next(readProductsSuccess(products))
        observer.complete()
      }).catch(err => console.log(err))
    })
  )),
)


export default readProductsEpic

import * as types from './types'


export const readProducts = (data) => ({
  type: types.READ_PRODUCTS,
  payload: data
})


export const readProductsFail = (data = {}, message = '', code = null) => ({
  type: types.READ_PRODUCTS_FAIL,
  errors: {
    data,
    message,
    code,
  },
})


export const readProductsSuccess = products => ({
  type: types.READ_PRODUCTS_SUCCESS,
  products,
})

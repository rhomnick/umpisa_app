// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const readProductsReducer = (state, action) => {
  switch (action.type) {
    case types.READ_PRODUCTS:
      return {
        ...state,

        readProductsForm: {
          ...state.readProductsForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingProducts: true,
          readProductsFailed: false,
          readProductsSucceeded: false,
        },
      }
    case types.READ_PRODUCTS_FAIL:
      return {
        ...state,

        readProductsForm: {
          ...state.readProductsForm,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          readingProducts: false,
          readProductsFailed: true,
          readProductsSucceeded: false,
        },
      }
    case types.READ_PRODUCTS_SUCCESS: {
      return {
        ...state,

        products: action.products,

        readProductsForm: {
          ...state.readProductsForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingProducts: false,
          readProductsFailed: false,
          readProductsSucceeded: true,
        },
      }
    }
    default:
      return state
  }
}


export default reduceReducers(readProductsReducer)

import * as types from './types'


export const createTodos = (data) => {
  return {
    type: types.CREATE_TODOS,
    payload: data
  }
}


export const createTodosFail = (data = {}, message = '', code = null) => ({
  type: types.CREATE_TODOS_FAIL,
  errors: {
    data,
    message,
    code,
  },
})


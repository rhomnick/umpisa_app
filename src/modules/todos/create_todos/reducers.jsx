// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const createTodosReducer = (state, action) => {
  switch (action.type) {
    case types.CREATE_TODOS:
      return {
        ...state,

        createTodosForm: {
          ...state.createTodosForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          createingTodos: true,
          createTodosFailed: false,
          createTodosSucceeded: false,
        },
      }
    case types.CREATE_TODOS_FAIL:
      return {
        ...state,

        createTodosForm: {
          ...state.createTodosForm,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          createingTodos: false,
          createTodosFailed: true,
          createTodosSucceeded: false,
        },
      }
    case types.CREATE_TODOS_SUCCESS: {
      return {
        ...state,

        todo: action.todo,

        createTodosForm: {
          ...state.createTodosForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          createingTodos: false,
          createTodosFailed: false,
          createTodosSucceeded: true,
        },
      }
    }
    default:
      return state
  }
}


export default reduceReducers(createTodosReducer)

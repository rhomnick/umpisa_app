import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import { normalize } from 'appModules/app/helper'
import moment from 'moment'

import { gql } from 'apollo-boost'
import client from 'appModules/app/apollo'

// Types
import { CREATE_TODOS } from './types'

// Action
import { createTodosSuccess } from '../read_todos/actions'


const createTodosEpic = action$ => action$.pipe(
  ofType(CREATE_TODOS),
  mergeMap(action => (
    Observable.create((observer) => {
      client.mutate({
        mutation: gql`
            mutation ($data: CreateRecordInput){
              createRecord(data:$data) {
                _id          
                      status
                      primaryImage
                      title
                      content
                      author{
                        _id
                        firstName
                        lastName
                      }

                    type{
                      _id
                      name
                      }
                    category{
                        _id
                        name
                      }
              }
            }        
        `,
        variables: action.payload,
      }).then((result) => {
        const todo = result.data.createRecord
        observer.next(createTodosSuccess(todo))
        observer.complete()
      }).catch(err => console.log(err))
    })
  )),
)


export default createTodosEpic

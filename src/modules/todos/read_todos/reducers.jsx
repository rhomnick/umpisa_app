// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const readTodosReducer = (state, action) => {
  switch (action.type) {
    case types.READ_TODOS:
      return {
        ...state,

        readTodosForm: {
          ...state.readTodosForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingTodos: true,
          readTodosFailed: false,
          readTodosSucceeded: false,
        },
      }
    case types.READ_TODOS_FAIL:
      return {
        ...state,

        readTodosForm: {
          ...state.readTodosForm,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          readingTodos: false,
          readTodosFailed: true,
          readTodosSucceeded: false,
        },
      }
    case types.CREATE_TODOS_SUCCESS: {
      let newTodo = { ...state.todos }
      newTodo.data[action.todo._id] = action.todo
      newTodo.result.unshift([action.todo._id])
      return {
        ...state,

        todos: newTodo,

        readTodosForm: {
          ...state.readTodosForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingTodos: false,
          readTodosFailed: false,
          readTodosSucceeded: true,
        },
      }
    }
    case types.READ_TODOS_SUCCESS: {
      return {
        ...state,

        todos: action.todos,

        readTodosForm: {
          ...state.readTodosForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          readingTodos: false,
          readTodosFailed: false,
          readTodosSucceeded: true,
        },
      }
    }
    default:
      return state
  }
}


export default reduceReducers(readTodosReducer)

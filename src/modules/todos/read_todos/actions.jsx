import * as types from './types'


export const readTodos = (data) => ({
  type: types.READ_TODOS,
  payload: data
})


export const readTodosFail = (data = {}, message = '', code = null) => ({
  type: types.READ_TODOS_FAIL,
  errors: {
    data,
    message,
    code,
  },
})


export const readTodosSuccess = todos => ({
  type: types.READ_TODOS_SUCCESS,
  todos,
})


export const createTodosSuccess = todo => ({
  type: types.CREATE_TODOS_SUCCESS,
  todo,
})
export const READ_TODOS = 'todos/read_todos/READ_TODOS'
export const READ_TODOS_FAIL = 'todos/read_todos/READ_TODOS_FAIL'
export const READ_TODOS_SUCCESS = 'todos/read_todos/READ_TODOS_SUCCESS'
export const CREATE_TODOS_SUCCESS = 'todos/create_todos/CREATE_TODOS_SUCCESS'

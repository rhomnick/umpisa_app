// Core
// import api from 'appModules/app/axios'
// import camelcaseKeys from 'camelcase-keys'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import { normalize } from 'appModules/app/helper'
import moment from 'moment'

import { gql } from 'apollo-boost'
import client from 'appModules/app/apollo'

// Types
import { READ_TODOS } from './types'

// Action
import { readTodosSuccess, readTodosFail } from './operations'


const readTodosEpic = action$ => action$.pipe(
  ofType(READ_TODOS),
  mergeMap(action => (
    Observable.create((observer) => {
      client.query({
        query: gql`
              query (
                $where: RecordsInput
                $limit : Int
                $offset : Int
              ) {
                getRecords( 
                  where: $where,
                  limit : $limit,
                  offset : $offset
                ) {
                  count
                  offset
                  limit
                  data {
                    _id          
                    status
                    primaryImage
                    title
                    content
                    author{
                      _id
                      firstName
                      lastName
                    }

                  type{
                    _id
                    name
                    }
                  category{
                      _id
                      name
                    }
                  }
                }
              }          
        `,
        variables: action.payload,
        fetchPolicy: 'network-only'
      }).then((result) => {
        const todosData = result.data.getRecords
        let todos = { result: [], data: {} }
        if (todosData && todosData.data)
          todos = normalize(todosData.data.map(todoData => ({
            ...todoData,
            dateCreated: moment(+todoData.createdAt).fromNow(),
          })), '_id')
        observer.next(readTodosSuccess(todos))
        observer.complete()
      }).catch(err => console.log(err))
    })
  )),
)


export default readTodosEpic

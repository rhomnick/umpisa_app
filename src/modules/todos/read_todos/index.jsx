import reducer from './reducers'

import * as readTodosOperations from './operations'

import readTodosEpic from './epic'


export {
  readTodosOperations,
  readTodosEpic,
}

export default reducer

// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import createTodosReducer from './create_todos'
import readTodosReducer from './read_todos'


// State shape
const initialState = {
  todos: {},
  
  createTodosForm: {
    // Error
    errors: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingTodos: false,
    readTodosFailed: false,
    readTodosSucceeded: false,
  },
  
  readTodosForm: {
    // Error
    errors: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingTodos: false,
    readTodosFailed: false,
    readTodosSucceeded: false,
  },
}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  readTodosReducer,
  createTodosReducer
)

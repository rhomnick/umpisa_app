// Core
import reduceReducers from 'reduce-reducers'

// Reducers
import createUserReducer from './create_user'
import readUserReducer from './read_user'

// State shape
const initialState = {
  user: {
    token: '',
  },

  createUserForm: {
    // Error
    errors: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    creatingUser: false,
    createUserFailed: false,
    createUserSucceeded: false,
  },

  readUserForm: {
    // Error
    errors: {
      message: '',
      data: {},
      code: null,
    },

    // Status
    readingUser: false,
    readUserFailed: false,
    readUserSucceeded: false,
  },
}

// Initial reducer
const initialReducer = (state = initialState) => (
  state
)

export default reduceReducers(
  initialReducer,
  createUserReducer,
  readUserReducer,
)


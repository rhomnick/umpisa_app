import * as types from './types'


export const readUser = (data) => ({
  type: types.READ_USER,
  payload: data
})

export const readUserProfile = () => ({
  type: types.READ_USER_PROFILE
})

export const readUserFail = (data = {}, message = '', code = null) => ({
  type: types.READ_USER_FAIL,
  errors: {
    data,
    message,
    code,
  },
})

export const readUserSuccess = user => ({
  type: types.READ_USER_SUCCESS,
  user: user,
})

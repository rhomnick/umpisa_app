import reducer from './reducers'

import * as readUserOperations from './operations'

import { readUserEpic, readUserProfileEpic } from './epic'


export {
  readUserOperations,
  readUserEpic,
  readUserProfileEpic
}

export default reducer

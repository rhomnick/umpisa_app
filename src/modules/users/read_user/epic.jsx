import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import { normalize } from 'appModules/app/helper'

import { gql } from 'apollo-boost'
import client from 'appModules/app/apollo'

// Types
import { READ_USER, READ_USER_PROFILE } from './types'

// Action
import { readUserSuccess, readUserFail } from './operations'


export const readUserEpic = action$ => action$.pipe(
  ofType(READ_USER),
  mergeMap(action => (
    Observable.create((observer) => {

      const { email, password } = action.payload
      client.mutate({
        mutation: gql`
          mutation {
            login(email: "${email}", password: "${password}") {
              user{
                _id
                firstName
                lastName
                email
                avatar
                analytics{
                  posts
                  products
                  todos
                }
              }
              slave
            }
          }             
        `,
      }).then((result) => {
        const { slave, user } = result.data.login
        localStorage.setItem('jwtToken', slave)
        observer.next(readUserSuccess(user))
        observer.complete()
      }).catch(err => console.log(err))
    })
  )),
)
export const readUserProfileEpic = action$ => action$.pipe(
  ofType(READ_USER_PROFILE),
  mergeMap(action => (
    Observable.create((observer) => {
      client.query({
        query: gql`
          query{
            profile {
              _id
              firstName
              lastName
              email
              avatar
              analytics{
                posts
                products
                todos
              }
            }
          }            
        `,
        fetchPolicy: 'network-only'
      }).then((result) => {
        const user = result.data.profile
        observer.next(readUserSuccess(user))
        observer.complete()
      }).catch(err => console.log(err))
    })
  )),
)




// Core
// import api from 'appModules/app/axios'
// import camelcaseKeys from 'camelcase-keys'
import { Observable } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { ofType } from 'redux-observable'
import { normalize } from 'appModules/app/helper'

import { gql } from 'apollo-boost'
import client from 'appModules/app/apollo'

// Types
import { CREATE_USER } from './types'

// Action
import { createUserSuccess, createUserFail } from './operations'


const createUserEpic = action$ => action$.pipe(
  ofType(CREATE_USER),
  mergeMap(action => (
    Observable.create((observer) => {
      const {
        email,
        password,
      } = action

      client.mutate({
        mutation: gql`
          mutation {
            register(email: "${email}", password: "${password}") {
              token
            }
          }             
        `,
      }).then((result) => {
        const { token } = result.data.register
        localStorage.setItem('jwtToken', token)
        observer.next(createUserSuccess(token))
        observer.complete()
      }).catch(err => console.log(err))
    })
  )),
)


export default createUserEpic

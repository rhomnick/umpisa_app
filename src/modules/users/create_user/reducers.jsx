// Core
import reduceReducers from 'reduce-reducers'

// Action types
import * as types from './types'


const createUserReducer = (state, action) => {
  switch (action.type) {
    case types.CREATE_USER:
      return {
        ...state,

        createUserForm: {
          ...state.createUserForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingUser: true,
          createUserFailed: false,
          createUserSucceeded: false,
        },
      }
    case types.CREATE_USER_FAIL:
      return {
        ...state,

        createUserForm: {
          ...state.createUserForm,

          error: {
            message: action.errors.message,
            data: action.errors.data,
            code: action.errors.code,
          },

          creatingUser: false,
          createUserFailed: true,
          createUserSucceeded: false,
        },
      }
    case types.CREATE_USER_SUCCESS: {
      return {
        ...state,
       
        user: {
          token: action.token,
        },

        createUserForm: {
          ...state.createUserForm,

          error: {
            message: '',
            data: {},
            code: null,
          },

          creatingUser: false,
          createUserFailed: false,
          createUserSucceeded: true,
        },
      }
    }
    default:
      return state
  }
}


export default reduceReducers(createUserReducer)

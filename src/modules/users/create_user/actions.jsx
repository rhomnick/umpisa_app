import * as types from './types'


export const createUser = () => ({
  type: types.CREATE_USER,
})


export const createUserFail = (data = {}, message = '', code = null) => ({
  type: types.CREATE_USER_FAIL,
  errors: {
    data,
    message,
    code,
  },
})


export const createUserSuccess = token => ({
  type: types.CREATE_USER_SUCCESS,
  token,
})

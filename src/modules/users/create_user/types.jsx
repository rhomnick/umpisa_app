export const CREATE_USER = 'users/create_user/CREATE_USER'
export const CREATE_USER_FAIL = 'users/create_user/CREATE_USER_FAIL'
export const CREATE_USER_SUCCESS = 'users/create_user/CREATE_USER_SUCCESS'
